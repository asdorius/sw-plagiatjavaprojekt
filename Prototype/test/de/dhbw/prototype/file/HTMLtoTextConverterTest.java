package de.dhbw.prototype.file;

import java.io.IOException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import de.dhbw.prototype.file.HTMLtoTextConverter;

@RunWith(JUnit4.class)
public class HTMLtoTextConverterTest {
	
	public void initializationError(){
		//TODO: What should happen?
	}
	
	@Test
	public void convertWithUrlString() throws IOException{
		String url = "www.chefkoch.de/rezepte/678051170241246/Birnen-Bananen-Marmelade.html";
		HTMLtoTextConverter.convertHTMLToText(url);
	}
	
	@Test
	public void convertWithUrl() throws IOException{
		String url = "www.chefkoch.de/rezepte/678051170241246/Birnen-Bananen-Marmelade.html";
		HTMLtoTextConverter.convertHTMLToText(new URL(url));
	}

}