package de.dhbw.prototype.db;

import java.sql.SQLException;

import de.dhbw.prototype.db.file.Folder;
import de.dhbw.prototype.db.user.User;

/**
 * Interface that defines the actions that are needed for an working user
 * management
 * 
 * @author Torsten Hopf
 *
 */
public interface UserManagement {
	/**
	 * Creates a new User
	 * 
	 * @param userToCreate
	 *            the user that should be created
	 * @throws SQLException
	 *             if the user can not be created or an database error occurs
	 */
	public void createNewUser(User userToCreate) throws SQLException;

	/**
	 * Returns the user with the given id, if one exist
	 * 
	 * @param id
	 *            the ID of the wished user
	 * @return the user if one exists, else null
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public User getUserById(int id) throws SQLException;

	/**
	 * Retunrs the user with the given email, if one exist
	 * 
	 * @param email
	 *            the email of the wished user
	 * @return the user if one exists, else null
	 * @throws SQLException
	 */
	public User getUserByEmail(String email) throws SQLException;

	/**
	 * Grants one or a group of users access to the given folder
	 * 
	 * @param folder
	 *            the folder to share
	 * @param users
	 *            the users allowed to see the folder's content
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public void grantUserAcessToFolder(Folder folder, User... users)
			throws SQLException;

	/**
	 * Method that checks if email and password do match
	 * 
	 * @param email
	 *            the users email
	 * @param password
	 *            the input password
	 * @return true if login data are correct, else false
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public boolean loginUser(String email, String password) throws SQLException;

	/**
	 * Deletes a existing User. The id is used to identify the right user
	 * 
	 * @param userToDelete
	 *            the user that should be deleted
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public void deleteUser(User userToDelete) throws SQLException;

	/**
	 * Updates a user's value. The id is used to identify the right user
	 * 
	 * @param userToUpdate
	 *            the user that should be updated
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public void updateUser(User userToUpdate) throws SQLException;
}
