package de.dhbw.prototype.db.role;

public class Action {
	private int id;
	private String name;
	
	public Action(){
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
