package de.dhbw.prototype.db.role;

import java.util.List;

public class Role {
	int id;
	int name;
	List<Action> actionsAllowedToPerform;
	
	public Role(int id, int name, List<Action> actionsAllowedToPerform) {
		this.id = id;
		this.name = name;
		this.actionsAllowedToPerform = actionsAllowedToPerform;
	}
	
	public Role(){
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getName() {
		return name;
	}
	public void setName(int name) {
		this.name = name;
	}
	public List<Action> getActionsAllowedToPerform() {
		return actionsAllowedToPerform;
	}
	public void setActionsAllowedToPerform(List<Action> actionsAllowedToPerform) {
		this.actionsAllowedToPerform = actionsAllowedToPerform;
	}
}
