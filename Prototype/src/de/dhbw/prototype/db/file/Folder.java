package de.dhbw.prototype.db.file;

import de.dhbw.prototype.db.user.User;
/**
 * Class that holds the data of a folder
 * @author Torsten Hopf
 *
 */
public class Folder {
	int id;
	String folderName;
	String token;
	int parentId;
	int ownerId;

	public Folder(int id, String folderName, String token, int parentId,
			int ownerId) {
		this.id = id;
		this.folderName = folderName;
		this.token = token;
		this.parentId = parentId;
		this.ownerId = ownerId;
	}

	public Folder(String folderName, String token, int parentId, int ownerId) {
		this.folderName = folderName;
		this.token = token;
		this.parentId = parentId;
		this.ownerId = ownerId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	
	public void setOwener(User owner){
		this.ownerId = owner.getId();
	}
}
