package de.dhbw.prototype.db.file;

import java.sql.Timestamp;
import java.util.Arrays;
/**
 * Class that holds the files data
 * @author Torsten Hopf
 *
 */
public class File {

	int id;
	String filename;
	String filetype;
	byte[] data;
	Timestamp timestamp;
	int parentId;
	int referenceId;

	
	public File(int id, String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId, int referenceId) {
		this.id = id;
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
		this.referenceId = referenceId;
	}

	public File(String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId, int referenceId) {
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
		this.referenceId = referenceId;
	}

	public File(String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId) {
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
	}

	public File(int id, String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId) {
		this.id = id;
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "File [id=" + id + ", filename=" + filename + ", filetype="
				+ filetype + ", data=" + Arrays.toString(data) + ", timestamp="
				+ timestamp + ", parentId=" + parentId + ", referenceId="
				+ referenceId + "]";
	}

	public int getReferenceId() {
		return referenceId;
	}
	
	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public Folder getParentFolder() {
		return null;

	}
	
	public void setParentFolder(Folder folder) {
		this.parentId = folder.getId();
	}
	
	public void setReference(Reference ref){
		this.referenceId = ref.getId();
	}

}
