package de.dhbw.prototype.db.file;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.prototype.db.DataBaseUtil;
import de.dhbw.prototype.db.FileManagement;
import de.dhbw.prototype.db.user.User;

/**
 * Implementation of the FileManagement Interface that uses a simple JDBC
 * Connection
 * 
 * @author Torsten Hopf
 *
 */
public class FileDBConnector implements FileManagement {

	@Override
	public boolean addFile(File file) throws SQLException {
		Connection cn = DataBaseUtil.getConnection();
		String sql = "INSERT INTO File ( Filename, Filetype, UploadDate, Parent,data) VALUES(?,?,?,?,?)";
		PreparedStatement stmt = cn.prepareStatement(sql);

		int filename = 1;
		int filetype = 2;
		int updateDate = 3;
		int parent = 4;
		int data = 5;

		try {

			stmt.setString(filename, file.getFilename());
			stmt.setString(filetype, file.getFiletype());
			stmt.setTimestamp(updateDate, file.getTimestamp());
			stmt.setInt(parent, file.getParentId());
			Blob blob = cn.createBlob();
			blob.setBytes(1, file.getData());
			stmt.setBlob(data, blob);

			stmt.executeUpdate();

			blob.free();

			return true;
		} catch (SQLException e) {
			cn.rollback();
			Notification.show("An SQL Error occured", Type.ERROR_MESSAGE);
			throw new SQLException("Could not insert File", e);
		} finally {
			cn.commit();
			stmt.close();
			DataBaseUtil.releaseConnection(cn);
		}

	}

	@Override
	public File getFileById(int fileId) throws SQLException {
		Connection cn = DataBaseUtil.getConnection();
		String sql = "SELECT id,Filename,Filetype,UploadDate,Parent,Reference,data From File WHERE id=?";
		PreparedStatement stmt = cn.prepareStatement(sql);
		int idNum = 1;
		ResultSet rs = null;
		stmt.setInt(idNum, fileId);
		try {
			rs = stmt.executeQuery();

			while (rs.next()) {
				Integer id = rs.getInt(1);
				String filename = rs.getString(2);
				String filetype = rs.getString(3);
				Timestamp uploadDate = rs.getTimestamp(4);
				Integer parentId = rs.getInt(5);
				Blob blob = rs.getBlob(7);
				int length = (int) blob.length();
				byte[] data = blob.getBytes(1, length);
				blob.free();

				// TODO: delete
				System.out.println(id);

				File file = new File(id, filename, filetype, data, uploadDate,
						parentId);
				return file;
			}
		} catch (SQLException e) {
			cn.rollback();
			Notification.show("An SQL Error occured", Type.ERROR_MESSAGE);
		} finally {
			cn.commit();
			rs.close();
			stmt.close();
			DataBaseUtil.releaseConnection(cn);
		}
		return null;
	}

	@Override
	public List<File> getFilesFromUser(User user) throws SQLException {

		return getFilesFromUser(user.getId());
	}

	@Override
	public List<File> getFilesFromUser(int userId) throws SQLException {
		// Connection cn = DataBaseUtil.getConnection();
		// String sql =
		// "SELECT id,Filename,Filetype,UploadDate,Parent,Reference,data From File WHERE owner=?";
		// PreparedStatement stmt = cn.prepareStatement(sql);
		// int idNum = 1;
		// ResultSet rs = null;
		// stmt.setInt(idNum, userId);
		// List<File> files = new ArrayList<File>();
		// try {
		// rs = stmt.executeQuery();
		// TODO: remove getFetchSize
		// if (rs.getFetchSize() != 0) {
		// while (rs.next()) {
		// Integer id = rs.getInt(1);
		// String filename = rs.getString(2);
		// String filetype = rs.getString(3);
		// Timestamp uploadDate = rs.getTimestamp(4);
		// Integer parentId = rs.getInt(5);
		// Blob blob = rs.getBlob(7);
		// int length = (int) blob.length();
		// byte[] data = blob.getBytes(0, length);
		//
		// File file = new File(id, filename, filetype, data,
		// uploadDate, parentId);
		// files.add(file);
		//
		// }
		// }
		// return files;
		// } catch (SQLException e) {
		// cn.rollback();
		// Notification.show("An SQL Error occured",Type.ERROR_MESSAGE);
		// } finally {
		// cn.commit();
		// rs.close();
		// stmt.close();
		// DataBaseUtil.releaseConnection(cn);
		// }
		return null;

		// throw new NotImplementedException("Not implemented yet!");

	}

	@Override
	public boolean deleteFile(File file) throws SQLException {
		return deleteFile(file.getId());

	}

	@Override
	public boolean deleteFile(int fileId) throws SQLException {
		Connection cn = DataBaseUtil.getConnection();
		String sql = "DELETE FROM File WHERE Id = ?";
		PreparedStatement stmt = cn.prepareStatement(sql);
		stmt.setInt(1, fileId);

		try {
			stmt.execute();
			return true;
		} catch (SQLException e) {
			cn.rollback();
			Notification.show("File could not be deleted", e.toString(),
					Type.ERROR_MESSAGE);
			throw new SQLException("File can not be deleted", e);

		} finally {
			cn.commit();
			stmt.close();
			DataBaseUtil.releaseConnection(cn);
		}

	}

	@Override
	public List<File> getFilesFromFolder(Folder folder) throws SQLException {
		return getFilesFromFolder(folder.getId());
	}

	@Override
	public List<File> getFilesFromFolder(Integer id) throws SQLException {

		Connection cn = DataBaseUtil.getConnection();
		String sql = "SELECT Id, Filename,FileType,UploadDate,data From File WHERE Parent = ?";
		PreparedStatement stmt = cn.prepareStatement(sql);
		stmt.setInt(1, id);
		ResultSet rs = null;
		try {
			rs = stmt.executeQuery();

			List<File> files = new ArrayList<File>();
			while (rs.next()) {
				int fileId = rs.getInt(1);
				String filename = rs.getString(2);
				String filetype = rs.getString(3);
				Timestamp uploadDate = rs.getTimestamp(4);
				Blob blob = rs.getBlob(5);
				byte[] data = blob.getBytes(1, (int) blob.length());
				blob.free();

				// TODO: remove
				System.out.println(filename);

				File file = new File(fileId, filename, filetype, data,
						uploadDate, id);
				files.add(file);
			}
			return files;

		} catch (SQLException e) {
			cn.rollback();
			e.printStackTrace();
			throw new SQLException("Could not get Files", e);
		} finally {
			cn.commit();
			rs.close();
			stmt.close();
			DataBaseUtil.releaseConnection(cn);

		}
	}

}
