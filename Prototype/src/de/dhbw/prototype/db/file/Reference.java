package de.dhbw.prototype.db.file;
/**
 * Class that holds a reference data
 * @author Torsten Hopf
 *
 */
public class Reference {
	
	int id;
	String name;
	String title;
	String adress;
	
	public Reference(String name, String title, String adress) {
		this.name = name;
		this.title = title;
		this.adress = adress;
	}

	public Reference(int id, String name, String title, String adress) {
		this.id = id;
		this.name = name;
		this.title = title;
		this.adress = adress;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public void setId(int id) {
		this.id = id;
	}

}
