package de.dhbw.prototype.db;

import java.sql.SQLException;
import java.util.List;

import de.dhbw.prototype.db.file.File;
import de.dhbw.prototype.db.file.Folder;
import de.dhbw.prototype.db.user.User;

/**
 * Interface that defines the methods that are needed for Database interaction
 * containing files.
 * 
 * @author Torsten Hopf
 *
 */
public interface FileManagement {
	/**
	 * Adds a new File to the database
	 * 
	 * @param file
	 *            file that should be added to the db
	 * @return true on success
	 * @throws SQLException
	 *             if an error occurs while insertion
	 */
	public boolean addFile(File file) throws SQLException;

	/**
	 * Selects the file from the database with the given Id.
	 * 
	 * @param fileId
	 *            Id of the chosen file
	 * @return the file if a file with the given Id exists, else null
	 * @throws SQLException
	 *             if an error occurs while selection
	 */
	public File getFileById(int fileId) throws SQLException;

	/**
	 * Returns all files that have the input User as owner.
	 * 
	 * @param user
	 *            the Owner of the files
	 * @return the files where the user is owner, if any exist, else null
	 * @throws SQLException
	 *             if an error occurs while selection
	 */
	public List<File> getFilesFromUser(User user) throws SQLException;

	/**
	 * Returns all files that have the the input user ID as owner id
	 * 
	 * @param userId
	 *            the ID of the owner
	 * @return a list of files which contains the owner's files
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public List<File> getFilesFromUser(int userId) throws SQLException;

	/**
	 * Returns all files that are in the given folder
	 * 
	 * @param folder
	 *            the folder that contains the wished files
	 * @return all files in the folder
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public List<File> getFilesFromFolder(Folder folder) throws SQLException;

	/**
	 * Returns all files that have the input ID as parent ID
	 * 
	 * @param id
	 *            id of the folder that contains the wished files
	 * @return all files that have the input ID as parent ID
	 * @throws SQLException
	 *             if an database error occurs
	 */
	public List<File> getFilesFromFolder(Integer id) throws SQLException;

	/**
	 * Deletes the given file, if it exists
	 * 
	 * @param file
	 *            the file that will be deleted
	 * @return true on success
	 * @throws SQLException
	 *             if the file does not exist or an database error occurs
	 */
	public boolean deleteFile(File file) throws SQLException;

	/**
	 * Deletes the file with the given id, if it exists
	 * 
	 * @param fileId
	 *            id of the file that will be deleted
	 * @return true on success
	 * @throws SQLException
	 *             if the file does not exist or an databse error occurs
	 */
	public boolean deleteFile(int fileId) throws SQLException;

}
