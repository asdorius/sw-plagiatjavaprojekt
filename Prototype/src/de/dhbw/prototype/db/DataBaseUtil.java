package de.dhbw.prototype.db;

import java.sql.Connection;
import java.sql.SQLException;

import com.vaadin.data.util.sqlcontainer.connection.SimpleJDBCConnectionPool;

/**
 * Singleton that handles the connection pooling
 * 
 * @author Torsten Hopf
 *
 */
public class DataBaseUtil {

	private static SimpleJDBCConnectionPool pool = null;

	/**
	 * Returns a database connection from the SimpleJDBCConnectionPool
	 * 
	 * @return a connection to the database
	 */
	public static Connection getConnection() {
		Connection cn = null;
		try {
			if (pool == null) {
				pool = new SimpleJDBCConnectionPool("com.mysql.jdbc.Driver",
						"jdbc:mysql://185.82.20.252:3306/CiteRightSandbox",
						"Application", "notAtroll", 2, 10);
			}
			cn = pool.reserveConnection();
			cn.setAutoCommit(false);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return cn;

	}

	/**
	 * Releases a connection and returns it to the pool
	 * 
	 * @param cn
	 *            the connection that should be released
	 */
	public static void releaseConnection(Connection cn) {
		pool.releaseConnection(cn);
	}
}
