package de.dhbw.prototype.db.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.vaadin.ui.Notification;

import de.dhbw.prototype.db.DataBaseUtil;
import de.dhbw.prototype.db.UserManagement;
import de.dhbw.prototype.db.file.Folder;

public class UserDBConnector implements UserManagement {

	@Override
	public void createNewUser(User userToCreate) throws SQLException {
		// prepare transaction
		Connection connection = DataBaseUtil.getConnection();
		String sql = "INSERT INTO user (firstname,lastname,email,password,role) VALUES (?,?,?,?,?)";
		int firstname = 1;
		int lastname = 2;
		int email = 3;
		int password = 4;
		int role = 5;
		PreparedStatement insertStatement = connection.prepareStatement(sql);
		insertStatement.setString(firstname, userToCreate.getFirstName());
		insertStatement.setString(lastname, userToCreate.getLastName());
		insertStatement.setString(email, userToCreate.getEmail());
		insertStatement.setString(password, userToCreate.getPassword());
		insertStatement.setInt(role, userToCreate.getRoleId());

		// start the transaction
		try {
			insertStatement.executeUpdate();
		} catch (SQLException e) {
			// rollback on error
			connection.rollback();
			throw new SQLException(e);
		} finally {
			// release connection
			connection.commit();
			insertStatement.close();
			DataBaseUtil.releaseConnection(connection);
		}

	}

	@Override
	public User getUserById(int id) throws SQLException {
		// prepare
		Connection connection = DataBaseUtil.getConnection();
		String sql = "SELECT id,firstname,lastname,email,password,role FROM user WHERE id = ?";
		PreparedStatement getUserStmt = connection.prepareStatement(sql);
		int idPos = 1;
		int firstnamePos = 2;
		int lastnamePos = 3;
		int emailPos = 4;
		int passwordPos = 5;
		int roleIdPos = 6;
		getUserStmt.setInt(1, id);
		ResultSet rs = null;
		// execute
		try {
			rs = getUserStmt.executeQuery();
			rs.next();
			id = rs.getInt(idPos);
			String firstname = rs.getString(firstnamePos);
			String lastname = rs.getString(lastnamePos);
			String email = rs.getString(emailPos);
			String password = rs.getString(passwordPos);
			int roleId = rs.getInt(roleIdPos);

			User user = new User(firstname, lastname, email, password, roleId);
			user.setId(id);
			return user;

		} catch (SQLException e) {
			connection.rollback();
			throw new SQLException("User does not exist!", e);
		} finally {
			connection.commit();
			rs.close();
			getUserStmt.close();
			DataBaseUtil.releaseConnection(connection);
		}

	}

	@Override
	public User getUserByEmail(String usersEmail) throws SQLException {
		// prepare
		Connection connection = DataBaseUtil.getConnection();
		String sql = "SELECT id,firstname,lastname,email,password,role FROM user WHERE email = ?";
		PreparedStatement getUserStmt = connection.prepareStatement(sql);
		int idPos = 1;
		int firstnamePos = 2;
		int lastnamePos = 3;
		int emailPos = 4;
		int passwordPos = 5;
		int roleIdPos = 6;
		getUserStmt.setString(1, usersEmail);
		ResultSet rs = null;
		// execute
		try {
			rs = getUserStmt.executeQuery();
			rs.next();
			int id = rs.getInt(idPos);
			String firstname = rs.getString(firstnamePos);
			String lastname = rs.getString(lastnamePos);
			String email = rs.getString(emailPos);
			String password = rs.getString(passwordPos);
			int roleId = rs.getInt(roleIdPos);

			User user = new User(firstname, lastname, email, password, roleId);
			user.setId(id);
			return user;

		} catch (SQLException e) {
			connection.rollback();
			throw new SQLException("User does not exist!", e);
		} finally {
			connection.commit();
			rs.close();
			getUserStmt.close();
			DataBaseUtil.releaseConnection(connection);
		}
	}

	@Override
	public void grantUserAcessToFolder(Folder folder, User... users)
			throws SQLException {
		// Prepare
		Connection cn = DataBaseUtil.getConnection();
		String sql = "INSERT INTO acess (User_ID,Folder_ID) VALUES (?,?)";
		PreparedStatement stmt = cn.prepareStatement(sql);
		try {
			// Execute
			for (User user : users) {
				stmt.setInt(1, user.getId());
				stmt.setInt(2, folder.getId());
				stmt.executeUpdate();
			}
		} catch (SQLException e) {
			throw (e);
		} finally {

		}
	}

	@Override
	public boolean loginUser(String email, String password) throws SQLException {
		User user = getUserByEmail(email);

		return (user.getPassword().equals(password));
	}

	@Override
	public void deleteUser(User userToDelete) throws SQLException {
		Connection cn = DataBaseUtil.getConnection();
		String sql = "DELETE FROM User WHERE ID = ?";
		PreparedStatement stmt = cn.prepareStatement(sql);

		try {
			stmt.setInt(1, userToDelete.getId());
			stmt.executeUpdate();
		} catch (SQLException e) {
			Notification.show("User couldn't be deleted");
			cn.rollback();
		}finally{
			cn.commit();
			stmt.close();
			DataBaseUtil.releaseConnection(cn);
		}

	}

	@Override
	public void updateUser(User userToUpdate) throws SQLException {
		
		Connection cn = DataBaseUtil.getConnection();
		String sql = "UPDATE User SET firstname=?,lastname=?,email=?,password=?,role=? WHERE id=?";
		int firstname = 1;
		int lastname = 2;
		int email = 3;
		int password = 4;
		int role = 5;
		int id = 6;
		
		PreparedStatement stmt = cn.prepareStatement(sql);
		stmt.setString(firstname, userToUpdate.getFirstName());
		stmt.setString(lastname, userToUpdate.getLastName());
		stmt.setString(email, userToUpdate.getEmail());
		stmt.setString(password, userToUpdate.getPassword());
		stmt.setInt(role, userToUpdate.getRoleId());
		stmt.setInt(id, userToUpdate.getId());
		
		try{
			stmt.executeUpdate();
		}catch(SQLException e){
			cn.rollback();
			throw(new SQLException("User could not be deleted",e));
		}finally{
			cn.commit();
			stmt.close();
			DataBaseUtil.releaseConnection(cn);
		}
		
		
	}

}
