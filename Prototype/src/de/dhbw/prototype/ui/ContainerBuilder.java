package de.dhbw.prototype.ui;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.prototype.db.file.File;
import de.dhbw.prototype.db.file.FileDBConnector;
import de.dhbw.prototype.ui.listener.FileUploadReceiver;

/**
 * Class that builds Containers that can be used for Tables, Lists and so on.
 * 
 * @author Torsten Hopf
 * @see com.vaadin.data.Container
 *
 */
public class ContainerBuilder {
	public static Object FILEID_PROPERTY = "ID";
	public static Object FILENAME_PROPERTY = "FILENAME";
	public static Object FILETYPE_PROPERTY = "FILETYPE";
	public static Object TIMESTAMP_PROPERTY = "TIMESTAMP";
	public static Object FOLDER_PROPERTY = "FOLDER";
	public static Object DATA_PROPERTY = "DATA";
	public static Object PARENTID_PROPERTY = "PARENT";
	public static Object DETAILS_BUTTON_PROPERTY = "BUTTON";

	/**
	 * Builds an IndexedContainer that contains the files data that can be found
	 * in the root directory.
	 * @see IndexedContainer
	 * @return the IndexedContainer if there are any files, else null
	 */
	@SuppressWarnings("unchecked")
	public static IndexedContainer getFileContainerFromRoot() {

		IndexedContainer container = new IndexedContainer();
		// add the propertys
		container.addContainerProperty(FILEID_PROPERTY, Integer.class, null);
		container.addContainerProperty(FILENAME_PROPERTY, String.class, null);
		container.addContainerProperty(FILETYPE_PROPERTY, String.class, null);
		container.addContainerProperty(TIMESTAMP_PROPERTY, Timestamp.class,
				null);
		container.addContainerProperty(DATA_PROPERTY, byte[].class, null);
		container.addContainerProperty(PARENTID_PROPERTY, Integer.class, null);
		container.addContainerProperty(DETAILS_BUTTON_PROPERTY, Button.class, null);
		FileDBConnector connector = new FileDBConnector();

		try {
			List<File> files = connector
					.getFilesFromFolder(FileUploadReceiver.ROOT);

			for (File file : files) {
				// set row's content
				Object itemId = container.addItem();
				container.getItem(itemId).getItemProperty(FILEID_PROPERTY)
						.setValue(file.getId());
				container.getItem(itemId).getItemProperty(FILENAME_PROPERTY)
						.setValue(file.getFilename());
				container.getItem(itemId).getItemProperty(FILETYPE_PROPERTY)
						.setValue(file.getFiletype());
				container.getItem(itemId).getItemProperty(TIMESTAMP_PROPERTY)
						.setValue(file.getTimestamp());
				container.getItem(itemId).getItemProperty(DATA_PROPERTY)
						.setValue(file.getData());
				container.getItem(itemId).getItemProperty(PARENTID_PROPERTY)
						.setValue(file.getParentId());
				Button detailsButton = new Button("Details");
				container.getItem(itemId).getItemProperty(DETAILS_BUTTON_PROPERTY).setValue(detailsButton);
				
				

			}
			return container;

		} catch (SQLException e) {
			Notification.show("Can't get any Fiels from root Dir",
					Type.ERROR_MESSAGE);
			e.printStackTrace();
		}

		return null;
	}

}
