package de.dhbw.prototype.ui.listener;

/**
 * Class that receives a file upload and writes the given data into the database.
 */
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import de.dhbw.prototype.db.file.File;
import de.dhbw.prototype.db.file.FileDBConnector;

@SuppressWarnings("serial")
public class FileUploadReceiver implements Receiver, SucceededListener,
		FailedListener {
	String filename;
	String mimeType;
	ByteArrayOutputStream output;
	Timestamp uploadTime;

	public static int ROOT = 2;

	@Override
	public void uploadFailed(FailedEvent event) {
		Notification.show("Upload fehlgeschlagen",
				event.getReason().toString(), Type.ERROR_MESSAGE);
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		if(filename.length() == 0){
			Notification.show("Schreiben in Datenbank fehlgeschlagen", Type.ERROR_MESSAGE);
			return;
		}

		byte[] data = output.toByteArray();
		File file = new File(filename, mimeType, data, uploadTime, ROOT);
		System.out.println(file.toString());
		FileDBConnector manager = new FileDBConnector();
		try {
			manager.addFile(file);
			Notification.show("Upload war Erfolgreich!");
		} catch (SQLException e) {
			e.printStackTrace();
			Notification.show("Schreiben in Datenbank fehlgeschlagen",
					e.toString(), Type.ERROR_MESSAGE);
		}

	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {

		this.filename = filename;
		this.mimeType = mimeType;
		this.uploadTime = new Timestamp(new Date().getTime());

		output = new ByteArrayOutputStream();

		return output;
	}

}
