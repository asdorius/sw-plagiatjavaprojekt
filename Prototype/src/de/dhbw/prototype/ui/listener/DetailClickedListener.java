package de.dhbw.prototype.ui.listener;

import java.sql.Timestamp;

import com.vaadin.data.Item;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;

import de.dhbw.prototype.db.file.File;
import de.dhbw.prototype.ui.ContainerBuilder;
import de.dhbw.prototype.ui.views.DetailedFileView;
import de.dhbw.prototype.ui.views.FileListView;

/**
 * Class that builds the file out of the Item's data and opens it in a new Tab
 * of the given TabSheet.
 * 
 * @author Torsten Hopf
 *
 */
public class DetailClickedListener implements ClickListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5799982789969084911L;
	private TabSheet tabSheet;
	private Item item;

	/**
	 * Constructor that sets the TabSheet which shall display the
	 * DetailedFileFiew
	 * 
	 * @param tabSheet
	 *            the tabSheet to display the new content
	 * @param item
	 *            the item that stores the file's data
	 */
	public DetailClickedListener(TabSheet tabSheet, Item item) {
		this.tabSheet = tabSheet;
		this.item = item;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		String filename = (String) item.getItemProperty(
				ContainerBuilder.FILENAME_PROPERTY).getValue();
		String filetype = (String) item.getItemProperty(
				ContainerBuilder.FILETYPE_PROPERTY).getValue();
		Timestamp timestamp = (Timestamp) item.getItemProperty(
				ContainerBuilder.TIMESTAMP_PROPERTY).getValue();
		byte[] data = (byte[]) item.getItemProperty(
				ContainerBuilder.DATA_PROPERTY).getValue();
		Integer id = (int) item.getItemProperty(
				ContainerBuilder.FILEID_PROPERTY).getValue();
		Integer parentId = (int) item.getItemProperty(
				ContainerBuilder.PARENTID_PROPERTY).getValue();

		File file = new File(id, filename, filetype, data, timestamp, parentId);

		Tab tab = tabSheet.addTab(new DetailedFileView(file));
		tab.setCaption(file.getFilename());
		tab.setClosable(true);
		tabSheet.setSelectedTab(tab);
	}

}
