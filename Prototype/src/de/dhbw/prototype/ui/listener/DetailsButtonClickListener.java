package de.dhbw.prototype.ui.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Link;
import com.vaadin.ui.UI;

import de.dhbw.prototype.search.SearchResult;
import de.dhbw.prototype.ui.PrototypeUI;
import de.dhbw.prototype.ui.views.CompareView;
import de.dhbw.prototype.ui.views.FileListView;
import de.dhbw.prototype.ui.views.FragmentCheckView;

public class DetailsButtonClickListener implements ClickListener {

	private SearchResult result;
	private String compareText;

	public DetailsButtonClickListener(SearchResult result, String compareText) {
		this.result = result;
		this.compareText = compareText;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		FileListView fileView = (FileListView) UI.getCurrent().getContent();
		Tab newTab = fileView.getTabsheet().addTab(new CompareView(result, compareText));
		newTab.setCaption("Fragmentprüfung");
		newTab.setClosable(true);
		fileView.getTabsheet().setSelectedTab(newTab);
	}

}
