package de.dhbw.prototype.ui.listener;

import com.vaadin.ui.AbstractField;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.UI;

import de.dhbw.prototype.ui.views.FileListView;
import de.dhbw.prototype.ui.views.FragmentCheckView;

/**
 * Listener that is invoked when the validate button is pressed
 * 
 * @author Torsten Hopf und Florian Knecht
 *
 */
public class ValidateListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1372887135435983515L;
	private AbstractField<String> sectionTextArea;
	private AbstractField<String> wholeTextArea;

	/**
	 * Constructer that uses an AbstractField as section for the
	 * FramentCheckView
	 * 
	 * @param section
	 *            the section to be set
	 */
	public ValidateListener(AbstractField<String> section, AbstractField<String> wholeText) {
		this.sectionTextArea = section;
		this.wholeTextArea = wholeText;
		
	}

	@Override
	public void buttonClick(ClickEvent event) {
		FileListView fileView = (FileListView) UI.getCurrent().getContent();
		Tab newTab = fileView.getTabsheet().addTab(
				new FragmentCheckView(sectionTextArea.getValue(),wholeTextArea.getValue()));
		newTab.setCaption("Fragmentprüfung");
		newTab.setClosable(true);
		fileView.getTabsheet().setSelectedTab(newTab);

	}

}
