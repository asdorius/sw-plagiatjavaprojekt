package de.dhbw.prototype.ui.listener;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.FileResource;
import com.vaadin.server.Resource;
import com.vaadin.server.VaadinService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Image;
import com.vaadin.ui.Link;
import com.vaadin.ui.TextField;

import de.dhbw.prototype.file.HTMLtoTextConverter;
import de.dhbw.prototype.search.SearchEngine;
import de.dhbw.prototype.search.SearchResult;
import de.dhbw.prototype.search.google.GoogleSearchEngine;

/**
 * Represents the result of a source check
 * @author Florian
 *
 */
enum SourceCheckResult {
	Match, PartlyMatch, NoMatch
}

/**
 * This class handles the click event of the button in FragmentCheckLayout
 * @author Florian
 *
 */
public class CheckButtonClickListener implements ClickListener {
	private TextField compareTextField;
	private IndexedContainer resultContainer;
	private String wholeText;

	/**
	 * Creates a new CheckButtonListener
	 * @param compareTextField The TextField that contains the text the search ist based upon
	 * @param resultContainer The IndexedContainer in which the results of the search will be stored
	 */
	public CheckButtonClickListener(TextField compareTextField,
			IndexedContainer resultContainer, String wholeText) {
		System.out.println(wholeText);
		this.compareTextField = compareTextField;
		this.resultContainer = resultContainer;
		this.wholeText = wholeText;
	}

	/**
	 * A GoogleSearch is performed and the results are being stored in the resultContainer
	 * @param event
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String compareText = compareTextField.getValue();
		List<SearchResult> searchResults = googleSearch(compareText);
		for (SearchResult result : searchResults) {
			URL url = null;
			try {
				url = new URL(result.getUrl());
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			fillResultContainer(compareText, result, sourceCheck(wholeText, url));
		}
	}

	/**
	 * Checks if the given url is wholely or partly present in the given checkText.
	 * @param checkText The text in which o search for the URL
	 * @param url The URL to look for
	 * @return The reult in form of a SourceCheckResult enumeration value
	 */
	private SourceCheckResult sourceCheck(String checkText, URL url) {
		if (url != null) {
			if (checkText.contains(url.getAuthority() + url.getPath())) {
				return SourceCheckResult.Match;
			}
			if (checkText.contains(url.getAuthority())) {
				return SourceCheckResult.PartlyMatch;
			}
		}
		return SourceCheckResult.NoMatch;

	}

	/**
	 * Performs a simple google search with the provided text
	 * @param compareText The text to search for
	 * @return
	 */
	private List<SearchResult> googleSearch(String compareText) {
		SearchEngine google = new GoogleSearchEngine();
		List<SearchResult> searchResults = new ArrayList();
		try {
			if (compareText != "") {
				searchResults = google.search(compareText, 5);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return searchResults;

	}

	/**
	 * Takes the results of all previous steps and fills the resultContainer with them
	 * @param compareText Needed to create the DetailsButtonClickListener
	 * @param result A SearchResult that is going t stored in the resultContainer
	 * @param sourceCheck The result of the source check
	 */
	private void fillResultContainer(String compareText, SearchResult result,
			SourceCheckResult sourceCheck) {

		Item item = resultContainer.getItem(resultContainer.addItem());
		
		item.getItemProperty("Title").setValue(
				HTMLtoTextConverter.removeTags(result.getTitle()));
		
		Link link = new Link(result.getUrl(), new ExternalResource(
				result.getUrl()));
		link.setTargetName("_blank");
		item.getItemProperty("Link").setValue(link);
		
		Button detailButton = new Button("Details",
				new DetailsButtonClickListener(result, compareText));
		item.getItemProperty("Details").setValue(detailButton);
		
		Image sourceCheckImage = determineImageFile(sourceCheck);
		item.getItemProperty("SourceCheck").setValue(sourceCheckImage);

	}

	/**
	 * Chooses the right image for to display the provided SourceCheckResult
	 * @param sourceCheck The SourceCheckREsult to base the Image choice upon
	 * @return An Image that diplays the given SourceCheckResul to the user
	 */
	private Image determineImageFile(SourceCheckResult sourceCheck) {
		String basepath = VaadinService.getCurrent()
	                  .getBaseDirectory().getAbsolutePath();
		switch (sourceCheck){
		case Match:{
			return new Image("Match", new FileResource(new File(basepath +
                    "/WEB-INF/images/Match.png")));
		}
		case PartlyMatch:{
			return new Image("Partly", new FileResource(new File(basepath +
                    "/WEB-INF/images/Partly.png")));
		}
		case NoMatch:{
			return new Image("NoMatch", new FileResource(new File(basepath +
                    "/WEB-INF/images/NoMatch.png")));
		}
		default:{
			return new Image("NoMatch", new FileResource(new File(basepath +
                    "/WEB-INF/images/NoMatch.png")));
		}
		}
	}

}
