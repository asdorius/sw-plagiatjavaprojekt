package de.dhbw.prototype.ui;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

import de.dhbw.prototype.ui.views.FileListView;

/**
 * The topmost component that contains all components and provides one user
 * interface per browser window
 * 
 * @author Torsten Hopf and Enrico Schrödter
 *
 */
@SuppressWarnings("serial")
@Theme("prototype")
public class PrototypeUI extends UI {

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = PrototypeUI.class, widgetset = "de.dhbw.prototype.ui.widgetset.PrototypeWidgetset")
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		FileListView flw = new FileListView();
		flw.setMargin(false);
		setContent(flw);
	}
}