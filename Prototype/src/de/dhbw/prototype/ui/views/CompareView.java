package de.dhbw.prototype.ui.views;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.RichTextArea;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.prototype.compare.section.Match;
import de.dhbw.prototype.compare.section.Section;
import de.dhbw.prototype.compare.section.converter.GroupOfWordsSectionsConverter;
import de.dhbw.prototype.compare.section.converter.LineSectionsConverter;
import de.dhbw.prototype.compare.section.converter.TextToSectionsConverter;
import de.dhbw.prototype.compare.section.sectionproxies.CompareWordsSectionProxy;
import de.dhbw.prototype.compare.section.sectionproxies.NoSpecialCharsSectionProxy;
import de.dhbw.prototype.compare.section.sectionproxies.NounSectionProxy;
import de.dhbw.prototype.db.file.File;
import de.dhbw.prototype.db.file.FileDBConnector;
import de.dhbw.prototype.file.HTMLtoTextConverter;
import de.dhbw.prototype.search.SearchResult;

/**
 * The CompareView compares two Texts and colores matches
 * @author entwickler
 */
public class CompareView extends GridLayout {
	private RichTextArea textAreaLeft;
	private RichTextArea textAreaRight;
	
	private Button searchButton;

	/**
	 * This Constructor initalisates the View
	 */
	public CompareView() {
		super(2, 1);
		this.setSizeFull();

		textAreaLeft = new RichTextArea("Links");
		textAreaLeft.setSizeFull();

		textAreaRight = new RichTextArea("Rechts");
		textAreaRight.setSizeFull();

		VerticalLayout left = new VerticalLayout();
		left.setMargin(true);
		left.addComponent(textAreaLeft);
		addComponent(left);

		VerticalLayout right = new VerticalLayout();
		right.setMargin(true);
		right.addComponent(textAreaRight);
		addComponent(right);
		
		/*

		final Container resultContainer = new IndexedContainer();
		resultContainer.addContainerProperty("Section1", String.class, "");
		resultContainer.addContainerProperty("Section2", String.class, "");
		resultContainer.addContainerProperty("Measurement", Double.class, 0);
		
		*/

		searchButton = new Button("Suchen", new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				TextToSectionsConverter converter = new GroupOfWordsSectionsConverter(5);

				List<Section> sections1 = converter.convert(textAreaLeft
						.getValue());
				List<Section> sections2 = converter.convert(textAreaRight
						.getValue());

				double barrier = 0.3;

				Map<Section, Match> results = new HashMap<Section, Match>();

				for (Section section1 : sections1) {
					for (Section section2 : sections2) {
						section1 = new NoSpecialCharsSectionProxy(new NounSectionProxy(
								section1));
						section2 = new NoSpecialCharsSectionProxy(new NounSectionProxy(
								section2));
						double measurement = new CompareWordsSectionProxy(section1).compare(section2);
						
						if (measurement > barrier && (!results.containsKey(section1) || results.get(section1).getMeasurement() < measurement) && !section1.getText().equals("") && !section2.getText().equals("")) {
							results.put(section1, new Match(section1, section2,
									measurement));
						}
					}
				}
				
				/*
				Set<Section> resultKeys = results.keySet();

				resultContainer.removeAllItems();

				for (Section key : resultKeys) {
					Match match = results.get(key);

					Item item = resultContainer.addItem(key.hashCode());
					item.getItemProperty("Section1").setValue(
							match.getSection1().getText());
					item.getItemProperty("Section2").setValue(
							match.getSection2().getText());
					item.getItemProperty("Measurement").setValue(
							match.getMeasurement());
				}
				
				*/
				
				String coloredText = colorTextSection(textAreaLeft.getValue(), results, "red");
				textAreaLeft.setValue(coloredText);
				textAreaLeft.setReadOnly(true);
				textAreaRight.setReadOnly(true);
			}
		});

		addComponent(searchButton);
		
		/*

		Table resultTable = new Table();
		resultTable.setSizeFull();

		resultTable.setContainerDataSource(resultContainer);

		addComponent(resultTable);
		
		*/
	}

	public CompareView(SearchResult result, String compareText) {
		this();

		try {
			String text = HTMLtoTextConverter.convertHTMLToText(result);
			textAreaLeft.setValue(text);
			textAreaRight.setValue(compareText);
			searchButton.click();
		} catch (IOException e) {
			//TODO: nützliche Fehlerbehandlung
			e.printStackTrace();
		}
	}

	/**
	 * Colors a text section
	 * 
	 * @param originalTextID
	 *            Is the ID from the file which should be colored
	 * @param matches
	 *            Is the match of the content from an other source
	 * @param colorCode
	 *            Is the color in which the text should be colored
	 * @return the text with the colored section
	 */
	private String colorTextSection(int originalTextID, Map<Section, Match> matches,
			String colorCode) {
		String originalTextString = "";
		try {
			File originalFile = new FileDBConnector()
					.getFileById(originalTextID);
			byte[] originalByteArray = originalFile.getData();
			originalTextString = originalByteArray.toString();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return colorTextSection(originalTextString, matches, colorCode);
	}
	
	/**
	 * Colors the text
	 * @param originalTextString
	 * @param matches
	 * @param colorCode
	 * @return colord text
	 */
	protected String colorTextSection(String originalTextString, Map<Section, Match> matches, String colorCode) {
		String coloredSection = "";
		Set<Section> matchesKeys = matches.keySet();
		for(Section key: matchesKeys){
			Section sectionToColor = matches.get(key).getSection1();
			String textToColor = sectionToColor.getSectionText();
			
			
			coloredSection = "<span style=\"background-color: " + colorCode
					+ ";\">" + textToColor + "</span>";
			originalTextString = originalTextString.replace(textToColor,
					coloredSection);
		}
		
		return originalTextString;
	}
}
