package de.dhbw.prototype.ui.views;

import com.vaadin.ui.Label;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.prototype.ui.listener.FileUploadReceiver;
/**
 * The UploadFileView shows a form to upload a File
 * @author entwickler
 */
public class UploadFileView extends VerticalLayout{
	public UploadFileView(){
		addComponent(new Label("UploadLayout"));
		FileUploadReceiver receiver = new FileUploadReceiver();
		Upload fileUpload = new Upload("", receiver);
		fileUpload.addSucceededListener(receiver);
		addComponent(fileUpload);
	}
}
