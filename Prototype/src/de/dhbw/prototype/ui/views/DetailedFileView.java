package de.dhbw.prototype.ui.views;

import com.vaadin.ui.Button;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.prototype.db.file.File;
import de.dhbw.prototype.ui.listener.ValidateListener;

/**
 * Class that presents the content of an uploaded Textfile
 * 
 * @author Moritz Ragg und Torsten Hopf
 *
 */
public class DetailedFileView extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4520410197362807250L;

	private TextArea textArea;
	private Button validateButton;
	private File file;

	/**
	 * Creates a new Layout to show the file's content in a RichTextField.
	 * 
	 * @param file
	 *            the file that should be shown
	 */
	public DetailedFileView(File file) {
		this.file = file;
		String text = new String(this.file.getData());

		textArea = new TextArea("Inhalt der Datei:");
		textArea.setValue(text);
		textArea.setReadOnly(true);
		textArea.setHeight(400, Unit.PIXELS);
		textArea.setWidth(100, Unit.PERCENTAGE);
		

		validateButton = new Button("Überprüfen");
		TextArea section = new TextArea("Textabschnitt, der überprüft werden soll.");
		section.setHeight(200, Unit.PIXELS);
		section.setWidth(500, Unit.PIXELS);
		validateButton.addClickListener(new ValidateListener(section,textArea));

		this.addComponent(textArea);
		this.addComponent(section);
		this.addComponent(validateButton);

	}

}
