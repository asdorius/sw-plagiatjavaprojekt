package de.dhbw.prototype.ui.views;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Image;
import com.vaadin.ui.Link;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.prototype.ui.listener.CheckButtonClickListener;

/**
 * The FragmentCheckView shows a form to search a Fragment of a Text in a Search Engine
 * @author entwickler
 *
 */
public class FragmentCheckView extends VerticalLayout{
	private TextField textField;
	private Button checkButton;
	private Table resultTable;
	private IndexedContainer resultContainer;
	private String wholeText;

	public FragmentCheckView(){
		this.setSizeFull();
		
		textField = new TextField();
		textField.setSizeFull();
		checkButton = new Button("Überprüfen");
		
		
		resultTable = new Table();
		resultTable.setWidth(100, Unit.PERCENTAGE);
		resultTable.setHeight(260,Unit.PIXELS);
		
		resultContainer = new IndexedContainer();
		resultContainer.addContainerProperty("Title", String.class, "");
		resultContainer.addContainerProperty("Link", Link.class, "");
		resultContainer.addContainerProperty("Details", Button.class, "");
		resultContainer.addContainerProperty("SourceCheck", Image.class, "");

		
		checkButton.addClickListener(new CheckButtonClickListener(textField, resultContainer, wholeText));
		resultTable.setContainerDataSource(resultContainer);
		
		addComponent(textField);
		addComponent(checkButton);
		addComponent(resultTable);
		
	}

	public FragmentCheckView(String value, String wholeText) {
		this.wholeText = wholeText;
this.setSizeFull();
		
		textField = new TextField();
		textField.setSizeFull();
		checkButton = new Button("Überprüfen");
		
		
		resultTable = new Table();
		resultTable.setWidth(100, Unit.PERCENTAGE);
		resultTable.setHeight(260,Unit.PIXELS);
		
		resultContainer = new IndexedContainer();
		resultContainer.addContainerProperty("Title", String.class, "");
		resultContainer.addContainerProperty("Link", Link.class, "");
		resultContainer.addContainerProperty("Details", Button.class, "");
		resultContainer.addContainerProperty("SourceCheck", Image.class, "");

		
		checkButton.addClickListener(new CheckButtonClickListener(textField, resultContainer, wholeText));
		resultTable.setContainerDataSource(resultContainer);
		
		addComponent(textField);
		addComponent(checkButton);
		addComponent(resultTable);
		
		textField.setValue(value);
		checkButton.click();
	}
}
