package de.dhbw.prototype.ui.views;

import com.vaadin.data.Container;
import com.vaadin.ui.Button;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TabSheet.SelectedTabChangeEvent;
import com.vaadin.ui.TabSheet.Tab;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.prototype.ui.ContainerBuilder;
import de.dhbw.prototype.ui.listener.DetailClickedListener;

/**
 * Class that contains a table which presents all uploaded files
 * 
 * @author Moritz Ragg
 * 
 */
public class FileListView extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -688840507548945903L;

	private Table table;
	private TabSheet tabSheet;
	private VerticalLayout fileList;

	/**
	 * Default Constructor that gets all files from the database and lists them
	 * in a Table
	 */
	public FileListView() {
		tabSheet = new TabSheet();
		
		fileList = getFileListLayout();

		Tab fileTab = tabSheet.addTab(fileList);
		fileTab.setCaption("Datei Liste");

		Tab uploadFileTab = tabSheet.addTab(new UploadFileView());
		uploadFileTab.setCaption("neue Datei hochladen");
		
		tabSheet.addListener(new TabSheet.SelectedTabChangeListener() {
			@Override
			public void selectedTabChange(SelectedTabChangeEvent event) {
				if(event.getTabSheet().getSelectedTab() instanceof FileListLayout){
					tabSheet.replaceComponent(event.getTabSheet().getSelectedTab(), getFileListLayout());
				}
			}
		});

		this.addComponent(tabSheet);

	}

	/**
	 * Returns the TabSheet member
	 * 
	 * @return the TabSheet
	 */
	public TabSheet getTabsheet() {
		return this.tabSheet;
	}
	
	private FileListLayout getFileListLayout(){
		FileListLayout fileList = new FileListLayout(tabSheet);
		
		return fileList;
	}
	
	/**
	 * The FileListLayout is a Layout that shows a Table with all Files
	 * @author entwickler
	 *
	 */
	private static class FileListLayout extends VerticalLayout{
		private Table table;
		
		FileListLayout(TabSheet tabSheet){
			table = new Table("Liste verfügbarer Textdateien in der Datenbank");
			table.setContainerDataSource(ContainerBuilder
					.getFileContainerFromRoot());
			table.setVisibleColumns(ContainerBuilder.FILENAME_PROPERTY,
					ContainerBuilder.TIMESTAMP_PROPERTY,
					ContainerBuilder.FILETYPE_PROPERTY,
					ContainerBuilder.DETAILS_BUTTON_PROPERTY);
			Container container = table.getContainerDataSource();
			for (Object id : container.getItemIds()) {
				Button button = (Button) container.getItem(id)
						.getItemProperty(ContainerBuilder.DETAILS_BUTTON_PROPERTY)
						.getValue();
				button.addClickListener(new DetailClickedListener(tabSheet,
						container.getItem(id)));

			}
			addComponent(table);
		}
	}
}
