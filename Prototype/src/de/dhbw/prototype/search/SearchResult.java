package de.dhbw.prototype.search;

/**
 * 
 * @author entwickler
 *	Data class for storing the results of the search engines
 */
public class SearchResult {
	
	private String title;
	private String url;
	
	/**
	 * Creates a new SearchResult with Title and URL
	 * @param title The title of the found webpage
	 * @param url The title of the found webpage
	 */
	public SearchResult(String title, String url){
		this.title = title;
		this.url= url;
	}
	
	/**
	 * 
	 * @return The saved title
	 */
	public String getTitle(){
		return title;
	}
	
	/**
	 * 
	 * @return The saved URL
	 */
	public String getUrl(){
		return url;
	}

}
