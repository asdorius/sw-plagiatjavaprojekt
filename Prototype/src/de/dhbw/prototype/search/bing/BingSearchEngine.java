package de.dhbw.prototype.search.bing;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;

import de.dhbw.prototype.search.SearchEngine;
import de.dhbw.prototype.search.SearchResult;

public class BingSearchEngine implements SearchEngine {

	private String mainURL = "https://api.datamarket.azure.com/Bing/Search/v1/Web?";
	private String charset = "UTF-8";

	String accountKeyEnc;

	public BingSearchEngine() {
		String accountKey = "eHmIF5nlGbVSmfQCi7blYFsbbgTO94nOXZoi/LcVioE";
		accountKeyEnc =new String(new Base64().encode((accountKey + ":" + accountKey).getBytes()));
	}

	@Override
	public List<SearchResult> search(String phrase) throws IOException {
		List<SearchResult> resultList = new ArrayList<SearchResult>();
		URL url = new URL(mainURL + createURL(phrase, 0));
		
		URLConnection connection = url.openConnection();
		connection
				.setRequestProperty("Authorization", "Basic " + accountKeyEnc);
		Reader reader = new InputStreamReader(connection.getInputStream(),
				charset);

		BingSearchResults results = new Gson().fromJson(reader,
				BingSearchResults.class);

		
		for (BingSearchResults.Result result : results.d.results) {
			resultList.add(new SearchResult(result.Title, result.Url));
		}

		return resultList;

	}

	@Override
	public List<SearchResult> search(String phrase, int limit)
			throws IOException {
		List<SearchResult> resultList = new ArrayList<SearchResult>();

		for (int i = 0; i < limit; i += 50) {
			URL url = new URL(mainURL + createURL(phrase, i));

			URLConnection connection = url.openConnection();
			connection.setRequestProperty("Authorization", "Basic "
					+ accountKeyEnc);
			Reader reader = new InputStreamReader(connection.getInputStream(),
					charset);

			BingSearchResults results = new Gson().fromJson(reader,
					BingSearchResults.class);
			int iterate = 0;
			if (limit - i < 50) {
				iterate = limit - i;
			} else {
				iterate = 50;
			}
			if (results.d.results.length < iterate){
				iterate = results.d.results.length;
			}
			for (int j = 0; j < iterate; j++) {
				String resultTitle = results.d.results[j].Title;
				String resultUrl = results.d.results[j].Url;
				resultList.add(new SearchResult(resultTitle, resultUrl));
			}

		}

		return resultList;

	}

	private String createURL(String phrase, int offset)
			throws UnsupportedEncodingException {
		return "Query=" + URLEncoder.encode("'" + phrase + "'", charset)
				+ "&$skip=" + offset + "&$format=json";
	}
}