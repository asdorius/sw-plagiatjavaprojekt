package de.dhbw.prototype.search;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import de.dhbw.prototype.search.google.GoogleSearchEngine;

/**
 * @author Moritz Ragg TestSearch Main-Class of a test system, which can start
 *         queries on google
 */
public class TestSearch {

	/**
	 * @param args
	 * @throws MalformedURLException
	 * @throws UnsupportedEncodingException
	 * @throws IOException
	 *             main-method: Creates a new instance of GoogleSearch and
	 *             starts a query depending on a console input The results are
	 *             readable on the console output
	 */
	public static void main(String[] args) throws MalformedURLException,
			UnsupportedEncodingException, IOException {
		SearchEngine googleSearch = new GoogleSearchEngine();
		List<SearchResult> list = new LinkedList<SearchResult>();

		Scanner scanner = new Scanner(System.in);
		System.out.println("Suche eingeben: ");
		String query = scanner.nextLine();
		System.out.println("Anzahl der Treffer (4-Schritte): ");
		String count = scanner.nextLine();

		System.out.println("Ergebnisse: \n-------------");

		list = googleSearch.search(query);

		for (SearchResult item : list) {
			System.out.println("Titel: \t "
					+ item.getTitle().replaceAll("(<b>|</b>)", ""));
			System.out.println("URL:\t " + item.getUrl() + " \n ");
		}
//		
//		list = new BingSearchEngine().search(query,5);
//
//		for (SearchResult item : list) {
//			System.out.println("Titel: \t "
//					+ item.getTitle().replaceAll("(<b>|</b>)", ""));
//			System.out.println("URL:\t " + item.getUrl() + " \n ");
//		}
	}
}
