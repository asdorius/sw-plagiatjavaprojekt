package de.dhbw.prototype.search.yacy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;

import de.dhbw.prototype.search.SearchEngine;
import de.dhbw.prototype.search.SearchResult;
import de.dhbw.prototype.search.yacy.YacySearchResults.Item;

public class YacySearchEngine implements SearchEngine {

	private String address = "http://asdorius.de:8090/yacysearch.json?";
	private String charset = "UTF-8";
	
	@Override
	public List<SearchResult> search(String phrase) throws IOException {
		YacySearchResults results;
		List<SearchResult> resultList = new LinkedList<SearchResult>();
		
		URL url = new URL(address +"query=" +URLEncoder.encode(phrase, charset));
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream(), charset));
		
	
					String bla = reader.readLine();
		String out = "";
		while(bla != null){
			out += bla;
			bla = reader.readLine();
		}
		
		results = new Gson().fromJson(out, YacySearchResults.class);
		

		
		for(Item result : results.items){
			resultList.add(new SearchResult(result.title, result.link));
		}
		
		return resultList;
	}

@Override
	public List<SearchResult> search(String phrase, int limit)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	
}
