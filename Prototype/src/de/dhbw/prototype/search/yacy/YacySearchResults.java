package de.dhbw.prototype.search.yacy;

import com.google.gson.annotations.SerializedName;


public class YacySearchResults {

	@SerializedName("items")
	public Item[] items;


	public static class Item {
		@SerializedName("title")
	    public String title;
		@SerializedName("description")
	    public String description;
		@SerializedName("link")
	    public String link;

	}
}
