package de.dhbw.prototype.search;

import java.util.List;
import java.io.IOException;


/**
 * 
 * Interface for Search-Classes
 * @author Moritz Ragg 
 */
public interface SearchEngine {

	public List<SearchResult> search(String phrase) throws IOException;

	public List<SearchResult> search(String phrase, int limit) throws IOException;
}
