package de.dhbw.prototype.search.google;

import java.util.LinkedList;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import com.google.gson.Gson;

import de.dhbw.prototype.search.SearchEngine;
import de.dhbw.prototype.search.SearchResult;
import de.dhbw.prototype.search.google.GoogleSearchResults.Result;

/**
 * @author morit_000 GoogleSearch Class for starting queries on google.com
 */
public class GoogleSearchEngine implements SearchEngine {
	private String address = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0";
	private String charset = "UTF-8";



	/**
	 * @return resultList creates a InputStreamReader which delivers the Json
	 *         objects of the results The Json objects are converted to
	 *         GoogleResults resultList contains these GoogleResults
	 * @throws IOException
	 */
	@Override
	public List<SearchResult> search(String phrase, int limit) throws IOException {
		GoogleSearchResults results;
		List<SearchResult> resultList = new LinkedList<SearchResult>();
		int iterate;

		for (int i = 0; i < limit; i += 4) {

			URL url = new URL(this.createURL(i)
					+ URLEncoder.encode(phrase + " filetype:html", charset));
			Reader reader = new InputStreamReader(url.openStream(), charset);
			results = new Gson().fromJson(reader, GoogleSearchResults.class);

			if (limit - i < 4) {
				iterate = limit - i;
			} else {
				iterate = 4;
			}
			if (results.getResponseData().getResults().size() < iterate){
				iterate = results.getResponseData().getResults().size();
			}

			for (int j = 0; j < iterate; j++) {
				String resultTitle = results.getResponseData().getResults().get(j).getTitle();
				String resultUrl = results.getResponseData().getResults().get(j).getUrl();
				resultList.add(new SearchResult(resultTitle, resultUrl));
			}
		}
		

		return resultList;
	}

	@Override
	public List<SearchResult> search(String phrase) throws IOException {
		GoogleSearchResults results;
		List<SearchResult> resultList = new LinkedList<SearchResult>();

		URL url = new URL(this.createURL(0)
				+ URLEncoder.encode(phrase, charset));
		Reader reader = new InputStreamReader(url.openStream(), charset);
		results = new Gson().fromJson(reader, GoogleSearchResults.class);

		
		
		for (Result result: results.getResponseData().getResults()){
			resultList.add(new SearchResult(result.getTitle(), result.getUrl()));
		}


		return resultList;
	}

	public String createURL(int i) {
		return address + "&start=" + i + "&q=";
	}

}
