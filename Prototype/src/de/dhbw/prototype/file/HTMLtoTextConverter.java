package de.dhbw.prototype.file;

import java.net.*;
import java.io.*;

import net.htmlparser.jericho.*;
import de.dhbw.prototype.search.*;

public class HTMLtoTextConverter {
	/**
	 * Takes the sourcecode from URL and converts it to an textfile.
	 * 
	 * @param url
	 *            URL of HTML document
	 * @throws IOException
	 */
	public static String convertHTMLToText(String url) throws IOException {
		Source htmlSource;
		Segment htmlSeg;
		Renderer htmlRend = null;

		Config.LoggerProvider = LoggerProvider.DISABLED;

		try {
			htmlSource = new Source(new URL(url));
			htmlSeg = new Segment(htmlSource, 0, htmlSource.length());
			htmlRend = new Renderer(htmlSeg);
			
			return htmlRend.toString();

		} catch (IOException e) {
			System.out.println("Die URL wurde nicht gefunden");
			e.printStackTrace();
		}
		
		return null;
	}

	/**
	 * Takes the sourcecode from URL and converts it to an textfile.
	 * 
	 * @param url
	 *            URL of HTML document
	 * @throws IOException
	 */
	public static String convertHTMLToText(URL url) throws IOException {
		return convertHTMLToText(url.toString());
	}

	/**
	 * Takes the sourcecode from URL given in result and converts it to an
	 * textfile.
	 * 
	 * @param result
	 *            SearchResult in which the URL is
	 * @throws IOException
	 */
	public static String convertHTMLToText(SearchResult result)
			throws IOException {
		return convertHTMLToText(result.getUrl());
	}

	/**
	 * Takes the HTML document and converts it to an textfile.
	 * 
	 * @param htmlFileStream
	 *            InputStream of an HTML document
	 * @param title
	 *            Title for the filename
	 * @throws IOException
	 */
	public static String convertHTMLToText(InputStream htmlFileStream,
			String title) throws IOException {
		Source htmlSource;
		Segment htmlSeg;
		Renderer htmlRend = null;

		Config.LoggerProvider = LoggerProvider.DISABLED;

		try {
			htmlSource = new Source(htmlFileStream);
			title = getTitle(htmlSource);
			htmlSeg = new Segment(htmlSource, 0, htmlSource.length());
			htmlRend = new Renderer(htmlSeg);
			
			return htmlRend.toString();

		} catch (IOException e) {
			System.out.println("Die URL wurde nicht gefunden");
			e.printStackTrace();
		} 	
		return null;
	}

	/**
	 * Takes the HTML document and converts it to an textfile.
	 * 
	 * @param htmlFile
	 *            HTML document
	 * @param title
	 *            Title for the filename
	 * @throws IOException
	 */
	public static String convertHTMLToText(String htmlFile, String title) {
		//TODO: Warum wird hier der Titel übergeben?
		Source htmlSource;
		Segment htmlSeg;
		Renderer htmlRend = null;

		Config.LoggerProvider = LoggerProvider.DISABLED;

		htmlSource = new Source(htmlFile);
		htmlSeg = new Segment(htmlSource, 0, htmlSource.length());
		htmlRend = new Renderer(htmlSeg);
		return htmlRend.toString(); 
		
	}
	
	/**
	 * Takes HTML-Markup and removes all Tags from it.
	 * @param htmlText 
	 * 				HTML-Markup
	 * @return
	 * 				String without Tags
	 */
	public static String removeTags(String htmlText){
		//TODO: Unterschied zwischen Convert und RemoveTags?
		Source htmlSource = new Source(htmlText);
		Segment htmlSeg = new Segment(htmlSource, 0, htmlSource.length());
		Renderer htmlRend = new Renderer(htmlSeg);
		return htmlRend.toString();
	}
	

	/**
	 * Extract the title out of the HTML Source.
	 * 
	 * @param htmlSource
	 *            is the sourcecode of the website
	 * @return returns the title of the website
	 */
	private static String getTitle(Source htmlSource) {
		Element titleElement = htmlSource
				.getFirstElement(HTMLElementName.TITLE);
		if (titleElement == null)
			return null;
		return CharacterReference.decodeCollapseWhiteSpace(titleElement
				.getContent());
	}

}