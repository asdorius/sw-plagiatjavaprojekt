package de.dhbw.prototype.compare.section;

/**
 * BaseSection is a simple Implementation of the Interface Section
 * Two Base Sections are can only be equal or not. (compare)
 * @author i13026
 */
public class BaseSection implements Section{
	private String text;
	private String sectionText;
	private int endPosition;
	private int startPosition;
	
	/**
	 * Sets the Text, the StartPostition and the EndPosition of the Section
	 * @param text
	 * @param startPosition
	 * @param endPosition
	 */
	public BaseSection(String text, int startPosition, int endPosition){
		this.sectionText = text;
		this.text = text;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
	}
	
	/**
	 * Returns only 0 or 1
	 * @see Section
	 */
	public double compare(Section section){
		if(getText().equals(section.getText())){
			return 1;
		}
		return 0;
	}

	@Override
	public String getText(){
		return text;
	}
	
	@Override
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int getStartPosition() {
		return startPosition;
	}

	@Override
	public int getEndPosition() {
		return endPosition;
	}
	
	/**
	 * Returns a String representation of the Object
	 * Only use the debug
	 * @return the Object as String
	 */
	public String toString(){
		return "{text: "+getText()+";startPosition: "+getStartPosition()+"; endPosition="+getEndPosition()+"}";
	}

	@Override
	public String getSectionText() {
		return sectionText;
	}
}
