package de.dhbw.prototype.compare.section.sectionproxies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.dhbw.prototype.compare.section.Section;
import de.dhbw.prototype.compare.section.SectionProxy;

/**
 * CompareWordsSection extends of Section Proxy and overrides the compare mehtod
 * It compares two Sections word by word
 * @author i13026
 */
public class CompareWordsSectionProxy extends SectionProxy{

	/**
	 * @see SectionProxy#SectionProxy(Section)
	 * @param section
	 */
	public CompareWordsSectionProxy(Section section) {
		super(section);
	}
	
	/**
	 * @see Section#compare(Section)
	 * Compares two Sections word by word 
	 * @param section
	 */
	@Override
	public double compare(Section section) {
		String currentString = getText();
		String givenString = section.getText();
		
		String[] currentStringWords = currentString.split(" ");
		String[] givenStringWords = givenString.split(" ");
		
		List<String> currentStringWordList =  new ArrayList<String>(Arrays.asList(currentStringWords));
		List<String> givenStringWordList =  new ArrayList<String>(Arrays.asList(givenStringWords));
		
		currentStringWordList.retainAll(givenStringWordList);
		
		return (2.0 * currentStringWordList.size())/(currentStringWords.length+givenStringWords.length);
	}
}
