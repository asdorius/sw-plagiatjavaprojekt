package de.dhbw.prototype.compare.section.sectionproxies;

import de.dhbw.prototype.compare.section.Section;
import de.dhbw.prototype.compare.section.SectionProxy;

/**
 * NoSpecialCharsSection extends SectionProxy ans overrides the getText Mehtod
 * It returns the Text without all specialchars
 * @author i13026
 */
public class NoSpecialCharsSectionProxy extends SectionProxy {

	/**
	 * @see SectionProxy#SectionProxy(Section)
	 * @param section
	 */
	public NoSpecialCharsSectionProxy(Section section) {
		super(section);
	}
	
	/**
	 * @see Section#getText()
	 * Returns the text without Specialchars
	 */
	@Override
	public String getText() {
		String text = super.getText();
		
		return text.replaceAll("[\\.\\,\\-\\:]", "");
	}

}
