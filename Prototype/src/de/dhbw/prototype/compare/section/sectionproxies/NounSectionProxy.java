package de.dhbw.prototype.compare.section.sectionproxies;

import de.dhbw.prototype.compare.section.Section;
import de.dhbw.prototype.compare.section.SectionProxy;

/**
 * NounSection extends of SectionProxy and overrides the getText Method
 * It returns only the nouns of the Text
 * @author i13026
 */
public class NounSectionProxy extends SectionProxy {
	/**
	 * @see SectionProxy#SectionProxy(Section)
	 * @param section
	 */
	public NounSectionProxy(Section section) {
		super(section);
	}
	
	/**
	 * @see Section#getText()
	 * Returns only the Nouns of the Text
	 */
	@Override
	public String getText() {
		//TODO: clever search of nouns
		String text = super.getText();
		String[] words = text.split(" ");
		
		StringBuilder stringBuilder = new StringBuilder();
		
		for(String word: words){
			try{
				if(word.charAt(0) == word.toUpperCase().charAt(0)){
					stringBuilder.append(word+" ");
				}
			}catch(IndexOutOfBoundsException e){
				
			}
		}
		
		if(stringBuilder.length() > 0){
			stringBuilder.substring(0,  stringBuilder.length()-1);
		}
		
		return stringBuilder.toString();
	}

}
