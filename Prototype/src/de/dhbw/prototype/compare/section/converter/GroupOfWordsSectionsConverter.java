package de.dhbw.prototype.compare.section.converter;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

/**
 * GroupOfWordsSectionConverter is an implementation of TextToSectionConvert,
 * that converts the Text into Sections of a few Words
 * @author i13026
 */
public class GroupOfWordsSectionsConverter implements TextToSectionsConverter{
	private int maxCountOfWords = 3;

	/**
	 * Sets the maximal count of words
	 * @param maxCountOfWords
	 */
	public GroupOfWordsSectionsConverter(int maxCountOfWords) {
		this.maxCountOfWords = maxCountOfWords;
	}
	
	/**
	 * Converts the Text into Sections of a few Words
	 * @see TextToSectionsConverter#convert(String)
	 */
	@Override
	public List<Section> convert(String text) {
		String[] words = text.split(" ");
		List<Section> sections = new LinkedList<Section>();
		
		String groupOfWords = "";
		int countOfWords = 0;
		int startPosition = 0;
		
		for(String word: words){
			if(countOfWords > 0){
				groupOfWords += " "+word;
			}else{
				groupOfWords += word;
			}
			
			countOfWords++;
			
			if(countOfWords >= maxCountOfWords){
				startPosition += groupOfWords.length();
				
				sections.add(new BaseSection(groupOfWords, startPosition, startPosition+groupOfWords.length()));
				
				countOfWords = 0;
				groupOfWords = "";
			}
		}
		
		if(countOfWords > 0){
			startPosition += groupOfWords.length();
			
			sections.add(new BaseSection(groupOfWords, startPosition, startPosition+groupOfWords.length()));
		}
		
		return sections;
	}

}
