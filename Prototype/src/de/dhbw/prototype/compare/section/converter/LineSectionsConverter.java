package de.dhbw.prototype.compare.section.converter;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

/**
 * LineSectionsConverter is an implementation of TextoSectionsonverter,
 * that converts the Text into Sections of Lines
 * @author i13026
 */
public class LineSectionsConverter implements TextToSectionsConverter {
	/**
	 * Converts the Text into Sections of Lines
	 * @see TextToSectionsConverter#convert(String)
	 */
	@Override
	public List<Section> convert(String text) {
		List<Section> sections = new LinkedList<>();
		
		String lineSeperator = "[\\r\\n]";
		
		String[] parts = text.split(lineSeperator);
		int startPosition = 0;
		
		for(String part: parts){
			sections.add(new BaseSection(part, startPosition, startPosition+part.length()));
			startPosition += part.length();
		}
		return sections;
	}
}
