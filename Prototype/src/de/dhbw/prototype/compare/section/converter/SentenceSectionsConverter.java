package de.dhbw.prototype.compare.section.converter;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

/**
 * SentenceSectionConverter is an implementation of TextToSectionsConverter
 * that converts the Text into Sections of Sentences
 * @author i13026
 */
public class SentenceSectionsConverter implements TextToSectionsConverter {

	/**
	 * Converts the Text into Sections of Sentences
	 * @see TextToSectionsConverter#convert(String)
	 */
	@Override
	public List<Section> convert(String text) {
		List<Section> sections = new LinkedList<>();
		
		String[] parts = text.split("\\.");
		
		int startPosition = 0;
		
		for(String part: parts){
			if(parts.length > 0){
				sections.add(new BaseSection(part, startPosition, startPosition+part.length()));
				startPosition += part.length()+1;
			}
		}
		return sections;
	}
}
