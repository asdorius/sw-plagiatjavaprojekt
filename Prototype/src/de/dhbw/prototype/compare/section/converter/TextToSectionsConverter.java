package de.dhbw.prototype.compare.section.converter;

import java.util.List;

import de.dhbw.prototype.compare.section.BaseSection;
import de.dhbw.prototype.compare.section.Section;

/**
 * Converts a Text as String into a List of Sections
 * @author i13026
 */
public interface TextToSectionsConverter {
	/**
	 * Takes a String and returns a List of Sections
	 * @param text
	 * @return The Sections of the Text
	 */
	public List<Section> convert(String text);
}
