package de.dhbw.prototype.compare.section;

public interface Section {	
	/**
	 * Compares a given Section with the current Section
	 * @param section
	 * @return double between 0 and 1
	 */
	public double compare(Section section);
	
	/**
	 * Returns the Text of the Section
	 * @return Text of the Section
	 */
	public String getText();
	
	/**
	 * Sets the Text of the Section
	 * @param the Text as String
	 */
	public void setText(String text);
	
	/**
	 * Returns the position of the first char of the section in the source text
	 * @return the startPosition
	 */
	public int getStartPosition();
	
	/**
	 * Returns the position of the last char of the section in the source text
	 * @return the entPosition
	 */
	public int getEndPosition();
	
	/**
	 * Returns the complete Text of the Section
	 * @return sectionText
	 */
	public String getSectionText();
}
