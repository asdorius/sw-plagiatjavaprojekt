package de.dhbw.prototype.compare.section;
/**
 * A Match is a combination of two Sections and a measurement.
 * The measurement says how much the first section equals the second Section
 * @author i13026
 */
public class Match {
	protected Section section1;
	protected Section section2;
	
	protected double measurement;
	
	/**
	 * Sets two  Sections and the Measurement
	 * @param section1
	 * @param section2
	 * @param measurement
	 */
	public Match(Section section1, Section section2, double measurement){
		this.section1 = section1;
		this.section2 = section2;
		this.measurement = measurement;
	}
	
	/**
	 * Returns the measurement
	 * @return a double between zero and one
	 */
	public double getMeasurement(){
		return measurement;
	}

	/**
	 * Returns the first section
	 * @return first section
	 */
	public Section getSection1() {
		return section1;
	}

	/**
	 * Returns the second section
	 * @return second section
	 */
	public Section getSection2() {
		return section2;
	}
	
	/**
	 * Returns the Match Object as String representation
	 * Only use to debug
	 * @return the Match as String
	 */
	public String toString(){
		return "{\"section1\" = \""+section1.toString()+"\", \"section2\" = \""+section2.toString()+"\", \"measurement\" = \""+measurement+"\"}";
	}
}
