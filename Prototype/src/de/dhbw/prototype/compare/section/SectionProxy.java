package de.dhbw.prototype.compare.section;

/**
 * A SectionProxy is implementation of Section,
 * that takes a Section and pass all calls of a method to this section
 * It should be extends to overwrite some methods
 * @author i13026
 */
public class SectionProxy implements Section {
	protected Section section;
	
	/**
	 * Sets the inner Section
	 * @param section
	 */
	public SectionProxy(Section section){
		this.section = section;
	}
	

	@Override
	public double compare(Section section) {
		return this.section.compare(section);
	}

	@Override
	public String getText() {
		return section.getText();
	}


	@Override
	public void setText(String text) {
		section.setText(text);
	}

	@Override
	public int getStartPosition() {
		return section.getStartPosition();
	}

	@Override
	public int getEndPosition() {
		return section.getEndPosition();
	}
	
	/**
	 * @see Section#toString()
	 */
	public String toString(){
		return section.toString();
	}


	@Override
	public final String getSectionText() {
		return section.getSectionText();
	}
}
